import unittest
from gis.src.Landsat_to_NDVI import *
from datetime import datetime
from gis.src.ImportLandsatImagery import *
import math



class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.fixtures = '/home/mark/Devel/gis/gis/tests/fixtures'

    def test_convert_to_julian(self):
        image_date = datetime(2001,10,18,18,51,26)
        image_date2 = datetime(2000, 1, 1, 12,0,0)
        jdn = convert_date_to_julian(image_date2)
        self.assertEqual(jdn, 2451545)

    def test_get_metadata(self):

        os.chdir(os.path.join(self.fixtures, 'L4'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L4'))
        self.assertEqual(meta_df['DATE_ACQUIRED'], '2008-05-07')
        self.assertEqual(meta_df['RADIANCE_MAXIMUM_BAND_3'], '264.000')
        self.assertEqual(meta_df['QUANTIZE_CAL_MAX_BAND_4'], '255')

    def test_l4_DN(self):
        test_tiff = os.path.join(self.fixtures, 'L4', 'test_L4_B3.tif')
        g = gdal.Open(test_tiff) # RED band
        RED = g.ReadAsArray()
        self.assertEqual(RED[2][1], 49)
        self.assertEqual(RED[-3][-2], 55)

    def test_l7_DN(self):
        test_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B3.tif')
        g = gdal.Open(test_tiff) # RED band
        RED = np.array(g.ReadAsArray())
        self.assertEqual(RED[2][1], 56)
        self.assertEqual(RED[-3][-2], 65)

    def test_l7_dn_to_ndvi(self):
        b3_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B3.tif')
        b4_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B4.tif')
        os.chdir(os.path.join(self.fixtures, 'L7'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L7'))
        distance = 1.011447059
        red_solar_irradiance = 1558
        nir_solar_irradiance = 1047
        sun_elevation = 64.38121616
        g = gdal.Open(b3_tiff) # RED band
        RED = g.ReadAsArray()
        g = gdal.Open(b4_tiff) # RED band
        NIR = g.ReadAsArray()
        RED_radiance = float(meta_df['RADIANCE_MULT_BAND_3']) * RED[2][1] + float(meta_df['RADIANCE_ADD_BAND_3'])
        NIR_radiance = float(meta_df['RADIANCE_MULT_BAND_4']) * NIR[2][1] + float(meta_df['RADIANCE_ADD_BAND_4'])
        self.assertAlmostEqual(RED_radiance, 46.86548)
        self.assertAlmostEqual(NIR_radiance, 61.76071)

        RED_reflectance = (math.pi * RED_radiance * math.pow(distance,2))/(red_solar_irradiance * math.cos(math.radians(sun_elevation)))
        NIR_reflectance = (math.pi * NIR_radiance * math.pow(distance,2))/(nir_solar_irradiance * math.cos(math.radians(sun_elevation)))
        self.assertAlmostEqual(RED_reflectance, 0.2235912557)
        self.assertAlmostEqual(NIR_reflectance, 0.4384648639)


    def test_l4_dn_to_ndvi(self):
        # testing calculation of radiance, reflectance and ndvi for L4/5 images
        # calculations were done in 'test_gis_calculations.ods'
        b3_tiff = os.path.join(self.fixtures, 'L4', 'test_L4_B3.tif')
        b4_tiff = os.path.join(self.fixtures, 'L4', 'test_L4_B4.tif')
        os.chdir(os.path.join(self.fixtures, 'L4'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L4'))
        g = gdal.Open(b3_tiff) # RED band
        RED = g.ReadAsArray()
        g = gdal.Open(b4_tiff) # RED band
        NIR = g.ReadAsArray()
        red_dn = float(RED[2][1]) # 49
        nir_dn = float(NIR[2][1]) # 70
        rad_max_3 = float(meta_df['RADIANCE_MAXIMUM_BAND_3'])
        rad_min_3 = float(meta_df['RADIANCE_MINIMUM_BAND_3'])
        qcal_max_3 = float(meta_df['QUANTIZE_CAL_MAX_BAND_3'])
        qcal_min_3 = float(meta_df['QUANTIZE_CAL_MIN_BAND_3'])
        rad_max_4 = float(meta_df['RADIANCE_MAXIMUM_BAND_4'])
        rad_min_4 = float(meta_df['RADIANCE_MINIMUM_BAND_4'])
        qcal_max_4 = float(meta_df['QUANTIZE_CAL_MAX_BAND_4'])
        qcal_min_4 = float(meta_df['QUANTIZE_CAL_MIN_BAND_4'])

        # radiance calculations
        red_radiance = (rad_max_3 - rad_min_3)/(qcal_max_3 - qcal_min_3) * (red_dn - qcal_min_3) + rad_min_3
        nir_radiance = (rad_max_4 - rad_min_4)/(qcal_max_4 - qcal_min_4) * (nir_dn - qcal_min_4) + rad_min_4
        self.assertAlmostEqual(red_radiance, 48.9408661417)
        self.assertAlmostEqual(nir_radiance, 58.9356299213)

        # reflectance calculations
        distance = 1.00925
        sun_elevation = 62.19501616
        red_solar_irradiance = 1558
        nir_solar_irradiance = 1047

        red_reflectance = (math.pi * red_radiance * math.pow(distance, 2))/(red_solar_irradiance * math.cos(math.radians(sun_elevation)))
        nir_reflectance = (math.pi * nir_radiance * math.pow(distance, 2))/(nir_solar_irradiance * math.cos(math.radians(sun_elevation)))

        self.assertAlmostEqual(red_reflectance, 0.215493335)
        self.assertAlmostEqual(nir_reflectance, 0.3861543156)

        ndvi = (nir_reflectance - red_reflectance)/(nir_reflectance + red_reflectance)
        self.assertAlmostEqual(ndvi, 0.2836560242)

    def test_calc_qcal_radiance(self):
        test_tiff = os.path.join(self.fixtures, 'L4', 'test_L4_B3.tif')
        g = gdal.Open(test_tiff) # RED band
        RED = np.array(g.ReadAsArray())
        test_tiff = os.path.join(self.fixtures, 'L4', 'test_L4_B4.tif')
        g = gdal.Open(test_tiff) # RED band
        NIR = np.array(g.ReadAsArray())
        os.chdir(os.path.join(self.fixtures, 'L4'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L4'))

        rad_max_red = float(meta_df['RADIANCE_MAXIMUM_BAND_3'])
        rad_min_red = float(meta_df['RADIANCE_MINIMUM_BAND_3'])
        qcal_min_red = float(meta_df['QUANTIZE_CAL_MIN_BAND_3'])
        qcal_max_red = float(meta_df['QUANTIZE_CAL_MAX_BAND_3'])
        rad_max_nir = float(meta_df['RADIANCE_MAXIMUM_BAND_4'])
        rad_min_nir = float(meta_df['RADIANCE_MINIMUM_BAND_4'])
        qcal_min_nir = float(meta_df['QUANTIZE_CAL_MIN_BAND_4'])
        qcal_max_nir = float(meta_df['QUANTIZE_CAL_MAX_BAND_4'])
        rad_max_array= [rad_max_red, rad_max_nir]
        rad_min_array = [rad_min_red, rad_min_nir]
        qcal_max_array = [qcal_max_red, qcal_max_nir]
        qcal_min_array = [qcal_min_red, qcal_min_nir]
        red_radiance, nir_radiance = calculate_qcal_radiance(RED, NIR, rad_max_array, rad_min_array, qcal_max_array, qcal_min_array)
        self.assertAlmostEqual(red_radiance[0][1], 43.720984252)
        self.assertAlmostEqual(nir_radiance[0][1], 59.8116535433)

    def test_calc_radiance(self):
        b3_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B3.tif')
        b4_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B4.tif')
        os.chdir(os.path.join(self.fixtures, 'L7'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L7'))
        rad_mult_red = float(meta_df['RADIANCE_MULT_BAND_3'])
        rad_mult_nir = float(meta_df['RADIANCE_MULT_BAND_4'])
        rad_add_red = float(meta_df['RADIANCE_ADD_BAND_3'])
        rad_add_nir = float(meta_df['RADIANCE_ADD_BAND_4'])
        rad_mult_array= [rad_mult_red, rad_mult_nir]
        rad_add_array = [rad_add_red, rad_add_nir]
        g = gdal.Open(b3_tiff) # RED band
        RED = g.ReadAsArray()
        g = gdal.Open(b4_tiff) # RED band
        NIR = g.ReadAsArray()

        RED_radiance, NIR_radiance = calculate_radiance(RED, NIR, rad_mult_array, rad_add_array)
        self.assertAlmostEqual(RED_radiance[0][1], 40.26448)
        self.assertAlmostEqual(NIR_radiance[0][1], 59.82271)

    def test_calc_reflectance(self):
        b3_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B3.tif')
        b4_tiff = os.path.join(self.fixtures, 'L7', 'test_L7_B4.tif')
        os.chdir(os.path.join(self.fixtures, 'L7'))
        meta_df = get_metadata(os.path.join(self.fixtures, 'L7'))
        rad_mult_red = float(meta_df['RADIANCE_MULT_BAND_3'])
        rad_mult_nir = float(meta_df['RADIANCE_MULT_BAND_4'])
        rad_add_red = float(meta_df['RADIANCE_ADD_BAND_3'])
        rad_add_nir = float(meta_df['RADIANCE_ADD_BAND_4'])
        rad_mult_array= [rad_mult_red, rad_mult_nir]
        rad_add_array = [rad_add_red, rad_add_nir]
        g = gdal.Open(b3_tiff) # RED band
        RED = g.ReadAsArray()
        g = gdal.Open(b4_tiff) # RED band
        NIR = g.ReadAsArray()
        distance = 1.011447059
        red_solar_irradiance = 1558
        nir_solar_irradiance = 1047
        solar_irradiance = [red_solar_irradiance, nir_solar_irradiance]
        sun_elevation = 64.38121616
        RED_radiance, NIR_radiance = calculate_radiance(RED, NIR, rad_mult_array, rad_add_array)
        RED_reflectance, NIR_reflectance = calculate_reflectance(RED_radiance, NIR_radiance, distance, solar_irradiance, sun_elevation)
        self.assertAlmostEqual(RED_reflectance[0][1], 0.192098441)
        self.assertAlmostEqual(NIR_reflectance[0][1], 0.4247061991)

if __name__ == '__main__':
    unittest.main()