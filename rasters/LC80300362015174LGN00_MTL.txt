GROUP = L1_METADATA_FILE
  GROUP = METADATA_FILE_INFO
    ORIGIN = "Image courtesy of the U.S. Geological Survey"
    REQUEST_ID = "0501506234569_00018"
    LANDSAT_SCENE_ID = "LC80300362015174LGN00"
    FILE_DATE = 2015-06-23T22:11:53Z
    STATION_ID = "LGN"
    PROCESSING_SOFTWARE_VERSION = "LPGS_2.5.1"
  END_GROUP = METADATA_FILE_INFO
  GROUP = PRODUCT_METADATA
    DATA_TYPE = "L1T"
    ELEVATION_SOURCE = "GLS2000"
    OUTPUT_FORMAT = "GEOTIFF"
    SPACECRAFT_ID = "LANDSAT_8"
    SENSOR_ID = "OLI_TIRS"
    WRS_PATH = 30
    WRS_ROW = 36
    NADIR_OFFNADIR = "NADIR"
    TARGET_WRS_PATH = 30
    TARGET_WRS_ROW = 36
    DATE_ACQUIRED = 2015-06-23
    SCENE_CENTER_TIME = "17:19:40.3077053Z"
    CORNER_UL_LAT_PRODUCT = 35.64847
    CORNER_UL_LON_PRODUCT = -102.62028
    CORNER_UR_LAT_PRODUCT = 35.69849
    CORNER_UR_LON_PRODUCT = -100.03348
    CORNER_LL_LAT_PRODUCT = 33.50185
    CORNER_LL_LON_PRODUCT = -102.52828
    CORNER_LR_LAT_PRODUCT = 33.54804
    CORNER_LR_LON_PRODUCT = -100.00715
    CORNER_UL_PROJECTION_X_PRODUCT = 172200.000
    CORNER_UL_PROJECTION_Y_PRODUCT = 3951000.000
    CORNER_UR_PROJECTION_X_PRODUCT = 406500.000
    CORNER_UR_PROJECTION_Y_PRODUCT = 3951000.000
    CORNER_LL_PROJECTION_X_PRODUCT = 172200.000
    CORNER_LL_PROJECTION_Y_PRODUCT = 3712500.000
    CORNER_LR_PROJECTION_X_PRODUCT = 406500.000
    CORNER_LR_PROJECTION_Y_PRODUCT = 3712500.000
    PANCHROMATIC_LINES = 15901
    PANCHROMATIC_SAMPLES = 15621
    REFLECTIVE_LINES = 7951
    REFLECTIVE_SAMPLES = 7811
    THERMAL_LINES = 7951
    THERMAL_SAMPLES = 7811
    FILE_NAME_BAND_1 = "LC80300362015174LGN00_B1.TIF"
    FILE_NAME_BAND_2 = "LC80300362015174LGN00_B2.TIF"
    FILE_NAME_BAND_3 = "LC80300362015174LGN00_B3.TIF"
    FILE_NAME_BAND_4 = "LC80300362015174LGN00_B4.TIF"
    FILE_NAME_BAND_5 = "LC80300362015174LGN00_B5.TIF"
    FILE_NAME_BAND_6 = "LC80300362015174LGN00_B6.TIF"
    FILE_NAME_BAND_7 = "LC80300362015174LGN00_B7.TIF"
    FILE_NAME_BAND_8 = "LC80300362015174LGN00_B8.TIF"
    FILE_NAME_BAND_9 = "LC80300362015174LGN00_B9.TIF"
    FILE_NAME_BAND_10 = "LC80300362015174LGN00_B10.TIF"
    FILE_NAME_BAND_11 = "LC80300362015174LGN00_B11.TIF"
    FILE_NAME_BAND_QUALITY = "LC80300362015174LGN00_BQA.TIF"
    METADATA_FILE_NAME = "LC80300362015174LGN00_MTL.txt"
    BPF_NAME_OLI = "LO8BPF20150623154917_20150623172459.01"
    BPF_NAME_TIRS = "LT8BPF20150623155011_20150623172553.01"
    CPF_NAME = "L8CPF20150401_20150630.03"
    RLUT_FILE_NAME = "L8RLUT20150303_20431231v11.h5"
  END_GROUP = PRODUCT_METADATA
  GROUP = IMAGE_ATTRIBUTES
    CLOUD_COVER = 0.09
    CLOUD_COVER_LAND = 0.09
    IMAGE_QUALITY_OLI = 9
    IMAGE_QUALITY_TIRS = 9
    TIRS_SSM_POSITION_STATUS = "NOMINAL"
    ROLL_ANGLE = -0.001
    SUN_AZIMUTH = 114.52111146
    SUN_ELEVATION = 67.86875019
    EARTH_SUN_DISTANCE = 1.0163719
    GROUND_CONTROL_POINTS_VERSION = 2
    GROUND_CONTROL_POINTS_MODEL = 336
    GEOMETRIC_RMSE_MODEL = 7.434
    GEOMETRIC_RMSE_MODEL_Y = 5.091
    GEOMETRIC_RMSE_MODEL_X = 5.418
    GROUND_CONTROL_POINTS_VERIFY = 97
    GEOMETRIC_RMSE_VERIFY = 3.571
  END_GROUP = IMAGE_ATTRIBUTES
  GROUP = MIN_MAX_RADIANCE
    RADIANCE_MAXIMUM_BAND_1 = 735.77338
    RADIANCE_MINIMUM_BAND_1 = -60.76041
    RADIANCE_MAXIMUM_BAND_2 = 753.44092
    RADIANCE_MINIMUM_BAND_2 = -62.21940
    RADIANCE_MAXIMUM_BAND_3 = 694.28937
    RADIANCE_MINIMUM_BAND_3 = -57.33464
    RADIANCE_MAXIMUM_BAND_4 = 585.46393
    RADIANCE_MINIMUM_BAND_4 = -48.34780
    RADIANCE_MAXIMUM_BAND_5 = 358.27472
    RADIANCE_MINIMUM_BAND_5 = -29.58644
    RADIANCE_MAXIMUM_BAND_6 = 89.09966
    RADIANCE_MINIMUM_BAND_6 = -7.35788
    RADIANCE_MAXIMUM_BAND_7 = 30.03136
    RADIANCE_MINIMUM_BAND_7 = -2.48000
    RADIANCE_MAXIMUM_BAND_8 = 662.58405
    RADIANCE_MINIMUM_BAND_8 = -54.71640
    RADIANCE_MAXIMUM_BAND_9 = 140.02188
    RADIANCE_MINIMUM_BAND_9 = -11.56305
    RADIANCE_MAXIMUM_BAND_10 = 22.00180
    RADIANCE_MINIMUM_BAND_10 = 0.10033
    RADIANCE_MAXIMUM_BAND_11 = 22.00180
    RADIANCE_MINIMUM_BAND_11 = 0.10033
  END_GROUP = MIN_MAX_RADIANCE
  GROUP = MIN_MAX_REFLECTANCE
    REFLECTANCE_MAXIMUM_BAND_1 = 1.210700
    REFLECTANCE_MINIMUM_BAND_1 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_2 = 1.210700
    REFLECTANCE_MINIMUM_BAND_2 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_3 = 1.210700
    REFLECTANCE_MINIMUM_BAND_3 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_4 = 1.210700
    REFLECTANCE_MINIMUM_BAND_4 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_5 = 1.210700
    REFLECTANCE_MINIMUM_BAND_5 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_6 = 1.210700
    REFLECTANCE_MINIMUM_BAND_6 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_7 = 1.210700
    REFLECTANCE_MINIMUM_BAND_7 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_8 = 1.210700
    REFLECTANCE_MINIMUM_BAND_8 = -0.099980
    REFLECTANCE_MAXIMUM_BAND_9 = 1.210700
    REFLECTANCE_MINIMUM_BAND_9 = -0.099980
  END_GROUP = MIN_MAX_REFLECTANCE
  GROUP = MIN_MAX_PIXEL_VALUE
    QUANTIZE_CAL_MAX_BAND_1 = 65535
    QUANTIZE_CAL_MIN_BAND_1 = 1
    QUANTIZE_CAL_MAX_BAND_2 = 65535
    QUANTIZE_CAL_MIN_BAND_2 = 1
    QUANTIZE_CAL_MAX_BAND_3 = 65535
    QUANTIZE_CAL_MIN_BAND_3 = 1
    QUANTIZE_CAL_MAX_BAND_4 = 65535
    QUANTIZE_CAL_MIN_BAND_4 = 1
    QUANTIZE_CAL_MAX_BAND_5 = 65535
    QUANTIZE_CAL_MIN_BAND_5 = 1
    QUANTIZE_CAL_MAX_BAND_6 = 65535
    QUANTIZE_CAL_MIN_BAND_6 = 1
    QUANTIZE_CAL_MAX_BAND_7 = 65535
    QUANTIZE_CAL_MIN_BAND_7 = 1
    QUANTIZE_CAL_MAX_BAND_8 = 65535
    QUANTIZE_CAL_MIN_BAND_8 = 1
    QUANTIZE_CAL_MAX_BAND_9 = 65535
    QUANTIZE_CAL_MIN_BAND_9 = 1
    QUANTIZE_CAL_MAX_BAND_10 = 65535
    QUANTIZE_CAL_MIN_BAND_10 = 1
    QUANTIZE_CAL_MAX_BAND_11 = 65535
    QUANTIZE_CAL_MIN_BAND_11 = 1
  END_GROUP = MIN_MAX_PIXEL_VALUE
  GROUP = RADIOMETRIC_RESCALING
    RADIANCE_MULT_BAND_1 = 1.2155E-02
    RADIANCE_MULT_BAND_2 = 1.2446E-02
    RADIANCE_MULT_BAND_3 = 1.1469E-02
    RADIANCE_MULT_BAND_4 = 9.6715E-03
    RADIANCE_MULT_BAND_5 = 5.9185E-03
    RADIANCE_MULT_BAND_6 = 1.4719E-03
    RADIANCE_MULT_BAND_7 = 4.9610E-04
    RADIANCE_MULT_BAND_8 = 1.0945E-02
    RADIANCE_MULT_BAND_9 = 2.3131E-03
    RADIANCE_MULT_BAND_10 = 3.3420E-04
    RADIANCE_MULT_BAND_11 = 3.3420E-04
    RADIANCE_ADD_BAND_1 = -60.77256
    RADIANCE_ADD_BAND_2 = -62.23184
    RADIANCE_ADD_BAND_3 = -57.34611
    RADIANCE_ADD_BAND_4 = -48.35747
    RADIANCE_ADD_BAND_5 = -29.59236
    RADIANCE_ADD_BAND_6 = -7.35935
    RADIANCE_ADD_BAND_7 = -2.48050
    RADIANCE_ADD_BAND_8 = -54.72735
    RADIANCE_ADD_BAND_9 = -11.56537
    RADIANCE_ADD_BAND_10 = 0.10000
    RADIANCE_ADD_BAND_11 = 0.10000
    REFLECTANCE_MULT_BAND_1 = 2.0000E-05
    REFLECTANCE_MULT_BAND_2 = 2.0000E-05
    REFLECTANCE_MULT_BAND_3 = 2.0000E-05
    REFLECTANCE_MULT_BAND_4 = 2.0000E-05
    REFLECTANCE_MULT_BAND_5 = 2.0000E-05
    REFLECTANCE_MULT_BAND_6 = 2.0000E-05
    REFLECTANCE_MULT_BAND_7 = 2.0000E-05
    REFLECTANCE_MULT_BAND_8 = 2.0000E-05
    REFLECTANCE_MULT_BAND_9 = 2.0000E-05
    REFLECTANCE_ADD_BAND_1 = -0.100000
    REFLECTANCE_ADD_BAND_2 = -0.100000
    REFLECTANCE_ADD_BAND_3 = -0.100000
    REFLECTANCE_ADD_BAND_4 = -0.100000
    REFLECTANCE_ADD_BAND_5 = -0.100000
    REFLECTANCE_ADD_BAND_6 = -0.100000
    REFLECTANCE_ADD_BAND_7 = -0.100000
    REFLECTANCE_ADD_BAND_8 = -0.100000
    REFLECTANCE_ADD_BAND_9 = -0.100000
  END_GROUP = RADIOMETRIC_RESCALING
  GROUP = TIRS_THERMAL_CONSTANTS
    K1_CONSTANT_BAND_10 = 774.8853
    K1_CONSTANT_BAND_11 = 480.8883
    K2_CONSTANT_BAND_10 = 1321.0789
    K2_CONSTANT_BAND_11 = 1201.1442
  END_GROUP = TIRS_THERMAL_CONSTANTS
  GROUP = PROJECTION_PARAMETERS
    MAP_PROJECTION = "UTM"
    DATUM = "WGS84"
    ELLIPSOID = "WGS84"
    UTM_ZONE = 14
    GRID_CELL_SIZE_PANCHROMATIC = 15.00
    GRID_CELL_SIZE_REFLECTIVE = 30.00
    GRID_CELL_SIZE_THERMAL = 30.00
    ORIENTATION = "NORTH_UP"
    RESAMPLING_OPTION = "CUBIC_CONVOLUTION"
  END_GROUP = PROJECTION_PARAMETERS
END_GROUP = L1_METADATA_FILE
END
