'''
Created on Oct 26, 2012

@author: Owner
'''
'''

Five variables created: NIR_gain, Red_gain, NIR_bias, Red_bias, sun_elev.
BeautifulSoup.py must be installed to run this module.
Beautiful Soup documentation:
http://www.crummy.com/software/BeautifulSoup/bs4/doc/


    Import modules*************************************************************
    '''
from bs4 import BeautifulSoup
import os.path
import glob
from osgeo import gdal
import pandas as pd

def ImportLandsat8(band_num, dpath):
    '''
    User enters working directory. Metadata file opened.***********************
    '''
    os.chdir(dpath)#set working directory
#    
    for metaDname in glob.glob('*MTL.txt'):#get metadata file from the working directory
        print 'Metadata filename:'
        print metaDname
        print
    
    metaD = BeautifulSoup(open(metaDname).read())#use BS to read .dim
    f = open(metaDname)
    contents = f.read()
    lines = contents.split('\n')
    linest = [x.strip() for x in lines]
    groups = [x.strip().split(' = ') for x in linest]
    
    #append a NULL to any row with length less than 2 (like 'END')
    for idx,row in enumerate(groups):
        if len(row) < 2:
            groups[idx].append('NULL')
            
    group_tuples = [tuple(x) for x in groups]
    group_dict = dict(group_tuples)
    df = pd.Series(group_dict)
    '''
    Get calibration data to convert digital numbers to reflectance.************
    '''
    
    print 'Getting gain and bias values for DEIMOS NIR and Red bands.'
    print
    
    #Find gain for all bands and save NIR (b2) and red (b3) gain
    #values as variables (eval format)
    image_date = df['DATE_ACQUIRED']
    earth_sun_distance = float(df['EARTH_SUN_DISTANCE'])
   
    red_fname = df['FILE_NAME_BAND_4'].strip('"') #red
    nir_fname = df['FILE_NAME_BAND_5'].strip('"') #NIR
    print "Getting sun elevation"
    SunElevAll = df['SUN_ELEVATION']
    sun_zenith = (90.0 - float(SunElevAll))
    print "sun elevation = " 
    print sun_zenith
    rad_mult = float(df['RADIANCE_MULT_BAND_{0}'.format(band_num)])
    rad_add = float(df['RADIANCE_ADD_BAND_{0}'.format(band_num)])
    ref_mult = float(df['REFLECTANCE_MULT_BAND_{0}'.format(band_num)])
    ref_add = float(df['REFLECTANCE_ADD_BAND_{0}'.format(band_num)])
    print
    
    '''
    Open Red and NIR images from the working directory.*************************
    '''   
    fname = os.path.join(dpath,"NDVI_"+image_date+".tif")
    if os.path.exists(fname):
        os.remove(fname)
    
#   Get DEIMOS image
    print 'Retrieving Landsat image file'
    for landsat_image in glob.glob('*.TIF'):#get metadata file from the working directory
        print 'Landsat image filename:'
        print landsat_image
        print
    
   
    
    return image_date,earth_sun_distance,ref_mult,rad_mult, rad_add,ref_mult,ref_add, sun_zenith, landsat_image
   
   
#    
