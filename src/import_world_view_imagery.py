import pandas as pd
import numpy as np
from datetime import datetime
from burn_raster import add_clouds
from osgeo import gdal, gdalnumeric, ogr
from gis_meetup import *
import collections


def get_metadata(dpath):

    os.chdir(dpath)
    for metadata_file in glob.glob('*.IMD'):#get metadata file from the working directory
        print 'Metadata filename:'
        print metadata_file
        print

    f = open(metadata_file)
    contents = f.read()
    lines = contents.split('\n')
    linest = [x.strip() for x in lines]
    groups = [x.strip().split(' = ') for x in linest]

    get_band_r = False
    get_band_n = False
    get_map = False
    for idx,row in enumerate(groups):
        if row[0] == 'END_GROUP' and row[1] == 'BAND_R':
            get_band_r = False
        if row[0] == 'BEGIN_GROUP' and row[1] == 'BAND_R':
            get_band_r = True
        if get_band_r and row[0] == 'absCalFactor':
            red_absCalFactor = row[1]
        if get_band_r and row[0] == 'effectiveBandwidth':
            red_effectiveBandwidth = row[1]

        if row[0] == 'END_GROUP' and row[1] == 'BAND_N':
            get_band_n = False
        if row[0] == 'BEGIN_GROUP' and row[1] == 'BAND_N':
            get_band_n = True
        if get_band_n and row[0] == 'absCalFactor':
            nir_absCalFactor = row[1]
        if get_band_n and row[0] == 'effectiveBandwidth':
            nir_effectiveBandwidth = row[1]
        if row[0] == 'earliestAcqTime':
            acq_date, acq_time_string = row[1].split('T')
            acq_time_split = acq_time_string.split(':')
            acq_time_split[2] = acq_time_split[2][:-2].split('.')[0]
            acq_datetime = datetime(*list(np.array([map(int,acq_date.split('-')), map(int,acq_time_split)]).flatten()))
        if row[0]=='meanSunEl':
            solar_zenith_angle = 90 - float(row[1][:-1])

    group_dict = {'nir_absCalFactor': float(nir_absCalFactor[:-1]), 'nir_effectiveBandWidth': float(nir_effectiveBandwidth[:-1]),
                  'red_absCalFactor':float(red_absCalFactor[:-1]), 'red_effectiveBandWidth': float(red_effectiveBandwidth[:-1]),
                  'DATE_ACQUIRED':acq_datetime, 'solar_zenith_angle':solar_zenith_angle}

    meta_df = pd.Series(group_dict)
    return meta_df


if __name__=='__main__':
    import_world_view2('/home/mark/Devel/gis/gis/rasters/dg/055514544010_01/055514544010_01_P001_MUL')