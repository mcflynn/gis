__author__ = 'mark'

import unittest
import gis_meetup
import Landsat_to_NDVI
import os

class gis_test(unittest.TestCase):
    def setUp(self):
        self.raster = '/home/mark/Devel/gis/rasters/NDVI.tif'
        self.output_ndvi = 'NDVI.tif'
        self.shapefile_dir = '/home/mark/Devel/gis/shapefiles'
        self.shapefile = 'corn_field'
        self.output = '/home/mark/Devel/gis/output/corn'
        self.mask_value = None


    def test_ndvi(self):
        os.remove(self.raster)
        Landsat_to_NDVI.convert_landsat8_to_ndvi(self.output_ndvi)
        self.assertTrue(os.path.exists('/home/mark/Devel/gis/rasters/NDVI.tif'))

    def test_subset(self):
        gis_meetup.subset_by_shapefile(self.raster, self.shapefile_dir, self.shapefile,self.output,self.mask_value)
        self.assertTrue(os.path.exists('/home/mark/Devel/gis/output/corn.tif'))

if __name__ == '__main__':
    unittest.main()
