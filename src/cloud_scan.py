import glob
import os
from gis_meetup import *
import numpy as np

def cloud_scan(rootdir, shapefile_dir, threshold):
    """
    :param rootdir: root directory where rasters are located
    :param shapefile_dir: directory of project area shapefile
    :return: list of rasters with clouds in the scene
    """

    cloud_raster_list = []
    for subdir, dirs, files in os.walk(rootdir):
        for filename in files:
            if 'BQA' in filename and filename.endswith('.TIF'):
                pixel_array = subset_by_shapefile(os.path.join(subdir, filename),
                                                  shapefile_dir,
                                                  'bando_2015_project_area')
                if np.any(pixel_array > threshold):
                    cloud_raster_list.append(filename)
                    print pixel_array[pixel_array>threshold]
    return cloud_raster_list

if __name__=='__main__':
    cloud_list = cloud_scan('/home/mark/Devel/gis/gis/rasters', '/home/mark/Devel/gis/shapefiles', 20600)
    print cloud_list